/**
 * \file sdlManager.hpp
 * \brief SDLManager class, which handles the graphical display
 * and user interaction for the Pac-Man game.
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef SDL_MANAGER_HPP
#define SDL_MANAGER_HPP

#include <SDL.h>
#include <iostream>
#include "pacman.hpp"
#include "direction.hpp"
#include <memory>
using std::shared_ptr;

class SDLManager{
private:
public:
    /**
     * \fn SDLManager()
     * \brief Default constructor for the SDLManager class.
     */
    SDLManager();

    /**
     * \fn ~SDLManager()
     * \brief Destructor for the SDLManager class.
     */
    ~SDLManager();

    /**
     * \fn bool menu(bool *quit, int *choice) const
     * \brief Displays the main menu and gets the user's choice.
     * \param quit Pointer to a boolean variable that will be set to true if the user wants to quit the game.
     * \param choice Pointer to a variable that will store the user's choice (1 for "play", 2 for "quit").
     * \return True if the display was successful, false otherwise.
     */
    bool menu(bool *quit, int *choice) const;

    /**
     * \fn void game(bool *quit, Entity* pacman) const
     * \brief Starts the game.
     * \param quit Pointer to a boolean variable that will be set to true if the user wants to quit the game.
     * \param pacman Pointer to the Pacman object that will be controlled by the user.
     */
    void game(bool *quit, Entity* pacman) const;
};

#endif //SDL_MANAGER_HPP
