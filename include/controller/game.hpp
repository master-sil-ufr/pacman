/**
 * \file game.hpp
 * \brief This class represents the main structure of a Pac-Man-like game.
 * It includes a unique pointer to a Window object that handles the graphical display,
 * a unique pointer to an SDLManager object that manages user input and SDL events
 * and a unique pointer to a Scene object that represents the game scene.
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef GAME_HPP
#define GAME_HPP

#include <SDL.h>
#include <memory>
#include "window.hpp"
#include "map.hpp"
#include "sdlManager.hpp"
#include "pacman.hpp"
#include "scene.hpp"

using std::unique_ptr;
using std::make_unique;

class Game{
private:
    unique_ptr<Window> window_;
    unique_ptr<SDLManager> sdlManager_;
    unique_ptr<Scene> scene_;
public:
    /**
     * \fn Game()
     * \brief Constructs a new Game object.
     */
    Game();

    /**
     * \fn ~Game()
     * \brief Destroys the Game object and frees any resources used.
     */
    ~Game() = default;

    /**
     * \fn void start()
     * \brief Initializes the game and displays the main menu.
     * It also handles the user's input and starts the game loop.
    */
    void start();

    /**
     * \fn void launchGame()
     * \brief Launches the game loop and runs the current game scene until
     * the game is over or the player quits the game.
     */
    void launchGame();
};

#endif //GAME_HPP