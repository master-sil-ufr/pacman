/**
 * \file blinky.hpp
 * \brief
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef BLINKY_HPP
#define BLINKY_HPP

#include "ghost.hpp"
#include "position.hpp"

/**
 * \brief Class representing the Blinky ghost entity in the game.
 */
class Blinky : public Ghost {
private:
public:
    /**
     * \fn
     * \brief Default constructor for Blinky.
     */
    Blinky();

    /**
     * \fn
     * \brief Constructor for Blinky, with initial position.
     * \param p The initial position of Blinky.
     */
    Blinky(const Position<float>& p);

    /**
     * \fn
     * \brief Resets Blinky's attributes to their initial values.
     */
    virtual void reset() override;

    /**
     * \fn
     * \brief Returns the time (in game ticks) that Blinky should leave the ghost house.
     * \return The time (in game ticks) that Blinky should leave the ghost house.
     */
    static unsigned int getTimeToleaveHub() { return 0; }
};

#endif //BLINKY_HPP