/**
 * \file map.hpp
 * \brief Map class, which represents a Pac-Man game map and its related functionalities.
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef MAP_HPP
#define MAP_HPP

#include <unordered_map>
#include <vector>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <exception>
#include <cmath>
#include "tile.hpp"
#include "position.hpp"
#include "entity.hpp"

using std::string;
using std::vector;
using std::ifstream;
using std::out_of_range;

class Map final {
private:
    unsigned int tileSize_;
    unsigned int gridWidth_;
    unsigned int gridHeight_;
    unsigned int numberPellet_;
    unsigned int numberPowerPellet_;
    vector<vector<Tile>> grid_;
public:
    /**
     * \fn Map()
     * \brief Deleted default constructor.
     */
    Map() = delete;

    /**
     * \fn Map(unsigned int tileSize)
     * \brief Constructor of the Map class.
     * \param tileSize The size of a tile in pixels.
     */
    Map(unsigned int tileSize);

    /**
     * \fn ~Map()
     * \brief Default destructor of the Map class.
     */
    ~Map() = default;

    /**
     * \fn unsigned int getNumberPellet() const
     * \brief Returns the number of pellets on the map.
     * \return The number of pellets on the map.
     */
    unsigned int getNumberPellet() const;

    /**
     * \fn unsigned int getNumberPower() const
     * \brief Returns the number of power pellets on the map.
     * \return The number of power pellets on the map.
     */
    unsigned int getNumberPower() const;

    /**
     * \fn Tile getTileValue(const Position<int>& tilePosition) const
     * \brief Returns the tile value at the map position.
     * \param tilePosition The position of the tile on the map.
     * \return The tile value at the map position.
     */
    Tile getTileValue(const Position<int>& tilePosition) const;

    /**
     * \fn Tile getNextTileValue(const Direction& objectDirection, const Position<int>& tilePosition) const
     * \brief This function takes as argument the position of a tile on the map
     * and returns the value of the adjacent tile in the indicated direction
     * \param objectDirection The direction of the object's movement.
     * \param tilePosition The position of the tile.
     * \return The value of the adjacent tile in the indicated direction
     */
    Tile getNextTileValue(const Direction& objectDirection, const Position<int>& tilePosition) const;

    /**
     * \fn const vector<vector<Tile>>& getGrid()
     * \brief Returns the grid of the map.
     * \return The grid of the map.
     */
    const vector<vector<Tile>>& getGrid() const;

    /**
     * \fn unsigned int countTileInTheGrid(const Tile &tileValue) const
     * \brief Returns the number of tiles in the grid with the given value.
     * \param tileValue The value of the tile to count.
     * \return The number of tiles in the grid with the given value.
     */
    unsigned int countTileInTheGrid(const Tile &tileValue) const;

    /**
     * \fn unsigned int countPelletInTheGrid() const
     * \brief Returns the number of pellets in the grid.
     * \return The number of pellets in the grid.
     */
    unsigned int countPelletInTheGrid() const;

    /**
     * \fn unsigned int countPowerPelletInTheGrid() const
     * \brief Returns the number of power pellets in the grid.
     * \return The number of power pellets in the grid.
     */
    unsigned int countPowerPelletInTheGrid() const;

    /**
     * \fn void setTileValue(const Position<int>& tilePosition, const Tile& newValue)
     * \brief Set the value of the tile at the tile position on the map.
     * \param tilePosition The position of the tile.
     * \param newValue The new value of the tile.
     */
    void setTileValue(const Position<int>& tilePosition, const Tile& newValue);

    /**
     * \fn void setFruit()
     * \brief Sets the fruit object in a random tile that currently has a pellet.
     */
    void setFruit();

    /**
     * \fn void loadMap()
     * \brief Loads the map from a file.
     */
    void loadMap();

    /**
     * \fn void parseMap(string &line)
     * \brief Parses a given line from the map file and populates the grid
     * with the corresponding Tile objects.
     * \param line A reference to a string containing a single line from
     * the map file.
     */
    void parseMap(string &line);

    /**
     * \fn Position<int> mapPixelPositionToMapPosition(const Position<int> &pixelPosition) const
     * \brief Converts a pixel position on the map to a tile position in the grid.
     * \param pixelPosition The pixel position to convert.
     * \return The corresponding tile position in the grid.
     */
    Position<int> mapPixelPositionToMapPosition(const Position<int> &pixelPosition) const;

    /**
     * \fn Position<int> discretizePosition(const Position<float>& continuousPosition) const
     * \brief Converts a float position on the map to a pixel position.
     * \param continuousPosition The discretize position to convert.
     * \return Pixel position.
     */
    Position<int> discretizePosition(const Position<float>& continuousPosition) const;

    /**
     * \fn Position<float> calculateObjectCenterPosition(const Position<float>& continuousPosition, const unsigned int width, const unsigned int height) const
     * \brief Return the center position of an object.
     * \param continuousPosition The discretize position of the object.
     * \param width The width of the object.
     * \param height The height of the object.
     * \return discretize position.
     */
    Position<float> calculateObjectCenterPosition(const Position<float>& continuousPosition, const unsigned int width, const unsigned int height) const;

    /**
     * \fn Tile consumeTile(const Position<int> &tilePosition)
     * \brief Removes the pellet or power pellet at the given tile position and returns its Tile value.
     * \param tilePosition The position of the tile containing the pellet or power pellet.
     * \return The Tile value of the removed pellet or power pellet.
     */
    Tile consumeTile(const Position<int> &tilePosition);

    /**
     * \fn Position<float> getPositionOnMap(const Entity &entity) const
     * \brief Returns the position of the given entity on the map in floating-point coordinates.
     * \param ent The entity whose position is to be determined.
     * \return The position of the entity on the map in floating-point coordinates.
     */
    Position<float> getPositionOnMap(const Entity &entity) const;

    /**
     * \fn vector<Direction> allDirectionCanBeChoose(const Position<int>& position, const Direction& direction) const;
     * \brief Returns all the directions in which an entity can choose to move from the given position and direction.
     * \param pos The position of the entity.
     * \param dir The current direction of the entity.
     * \return A vector containing all the directions in which the entity can choose to move from the given position and direction.
     */
    vector<Direction> allDirectionCanBeChoose(const Position<int>& position, const Direction& direction) const;

    /**
     * \fn bool areAllPelletsCollected() const
     * \brief Checks if all the pellets have been collected.
     * \return True if all the pellets have been collected, false otherwise.
     */
    bool areAllPelletsCollected() const;

    /**
     * \fn bool canChangeDirection(const Position<int> &topPosition, const Position<int> &bottomPosition, const Direction &direction) const;
     * \brief Determines whether the given entity can change direction at the given tile positions and direction.
     * \param topPosition The position of the top of the entity.
     * \param bottomPosition The position of the bottom of the entity.
     * \param d The direction in which the entity wants to change.
     * \return True if the entity can change direction at the given position, false otherwise.
     */
    bool canChangeDirection(const Position<int> &topPosition, const Position<int> &bottomPosition, const Direction &direction) const;

    /**
     * \fn bool canMoveForward(const Position<int> &bottomPosition, const Direction &direction) const;
     * \brief Determines whether the given entity can move forward in the given direction from the given position.
     * \param bottomPosition The position of the bottom of the entity.
     * \param d The direction in which the entity wants to move.
     * \return True if the entity can move forward in the given direction from the given position, false otherwise.
     */
    bool canMoveForward(const Position<int> &bottomPosition, const Direction &direction) const;
};
#endif //MAP_HPP