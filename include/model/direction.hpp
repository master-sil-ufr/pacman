/**
 * \file entity.hpp
 * \brief
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef DIRECTION_HPP
#define DIRECTION_HPP

enum class Direction {
    Up,
    Down,
    Left,
    Right,
    None
};

#endif //DIRECTION_HPP
