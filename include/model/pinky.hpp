/**
 * \file pinky.hpp
 * \brief
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef PINKY_HPP
#define PINKY_HPP

#include "ghost.hpp"

/**
 * \brief Class representing the Pinky ghost entity in the game.
 */
class Pinky : public Ghost {
private:
public:
    /**
     * \brief Default constructor for Pinky.
     */
    Pinky();

    /**
     * \brief Constructor for Pinky, with initial position.
     * \param p The initial position of Pinky.
     */
    Pinky(const Position<float>& p);

    /**
     * \brief Resets Pinky's attributes to their initial values.
     */
    virtual void reset() override;

    /**
     * \brief Returns the time (in game ticks) that Pinky should leave the ghost house.
     * \return The time (in game ticks) that Pinky should leave the ghost house.
     */
    static unsigned int getTimeToleaveHub() {
        return 960;
    }
};

#endif //PINKY_HPP