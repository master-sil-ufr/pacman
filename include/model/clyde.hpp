/**
 * \file clyde.hpp
 * \brief
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef CLYDE_HPP
#define CLYDE_HPP

#include "ghost.hpp"
#include "position.hpp"

/**
 * \brief Class representing the Clyde ghost entity in the game.
 */
class Clyde : public Ghost {
private:
public:
    /**
     * \fn
     * \brief Default constructor for Clyde.
     */
    Clyde();

    /**
     * \fn
     * \brief Constructor for Clyde, with initial position.
     * \param p The initial position of Clyde.
     */
    Clyde(const Position<float>& p);

    /**
     * \fn
     * \brief Resets Clyde's attributes to their initial values.
     */
    virtual void reset() override;

    /**
     * \fn
     * \brief Returns the time (in game ticks) that Clyde should leave the ghost house.
     * \return The time (in game ticks) that Clyde should leave the ghost house.
     */
    static unsigned int getTimeToleaveHub() { return 9600; }
};

#endif //CLYDE_HPP