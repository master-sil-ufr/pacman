/**
 * \file ghost.hpp
 * \brief
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef GHOST_HPP
#define GHOST_HPP

#include "entity.hpp"
#include "position.hpp"
#include <string>
using std::string;


/**
 * \class Ghost
 * \brief Represents a ghost in the Pac-Man game.
 *
 * This class inherits from the Entity class and adds additional properties and behaviors specific to ghosts.
 */
class Ghost : public Entity{
protected:
public:
    /**
     * \brief Constructs a Ghost object with a given name and position.
     * \param name The name of the ghost.
     * \param p The initial position of the ghost.
     */
    Ghost(string name,const Position<float>& p);

    /**
     * \brief Constructs a Ghost object with a given name, position, state, and direction.
     * \param name The name of the ghost.
     * \param p The initial position of the ghost.
     * \param s The initial state of the ghost.
     * \param d The initial direction of the ghost.
     */
    Ghost(string name,const Position<float>& p, const State& s,const Direction& d);

    /**
     * \brief Resets the ghost to its initial state and position.
     */
    virtual void reset()=0;

    /**
     * \brief Handles the death of the ghost.
     *
     * This method is called when the ghost is eaten by Pac-Man. It sets the ghost's state to "Eaten".
     */
    virtual void die() override;
};

#endif //GHOST_HPP