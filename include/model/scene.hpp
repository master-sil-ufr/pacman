/**
 * \file scene.hpp
 * \brief
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef SCENE_HPP
#define SCENE_HPP

#include <vector>
#include <memory>
#include <variant>

#include "pacman.hpp"
#include "blinky.hpp"
#include "inky.hpp"
#include "pinky.hpp"
#include "clyde.hpp"

#include "map.hpp"
#include "timer.hpp"

using std::vector;
using std::shared_ptr;
using std::unique_ptr;
using std::make_shared;
using std::make_unique;
using std::variant;

class Scene final {
private:
    unique_ptr<Timer> timer_;
    unsigned int level_;
    unsigned int score_;
    unsigned int highScore_;
    bool gameOver_;
    vector<Entity*> entities;
    unique_ptr<Map> map_;
    State state_;
public:
    /**
    \brief Default constructor for the Scene class.
    */
    Scene();

    /**
    \brief Destructor for the Scene class.
    */
    ~Scene() = default;

    /**
    \brief Returns the current game level.
    \return The current game level.
    */
    unsigned int getLevel() const;

    /**
    \brief Returns the current player score.
    \return The current player score.
    */
    unsigned int getScore() const;

    /**
    \brief Returns the highest score achieved in the game.
    \return The highest score achieved in the game.
    */
    unsigned int getHighScore() const;

    /**
    \brief Returns a reference to the vector containing pointers to all entities in the scene.
    \return A reference to the vector containing pointers to all entities in the scene.
    */
    const std::vector<Entity*>& getEntities() const;

    /**
    \brief Returns a pointer to the entity with the given name.
    \param nameEntity The name of the entity to retrieve.
    \return A pointer to the entity with the given name, or nullptr if no entity with that name exists.
    */
    Entity* getEntity(std::string nameEntity) const;

    /**
    \brief Returns a const reference to the current game map.
    \return A const reference to the current game map.
    */
    const Map& getMap() const;

    /**
    \brief Sets the current player score.
    \param score The new player score.
    */
    void setScore(unsigned int score);

    /**
    \brief Sets the current game level.
    \param level The new game level.
    */
    void setLevel(unsigned int level);

    /**
    \brief Sets the highest score achieved in the game.
    \param highScore The new highest score achieved in the game.
    */
    void setHighScore(unsigned int highScore);

    /**
    \brief Sets the vector containing pointers to all entities in the scene.
    \param entities A reference to the new vector of entities.
    */
    void setEntities(const std::vector<Entity*>& entities);

    /**
    \brief Adds the given score to the current player score.
    \param score The score to add.
    */
    void addScore(unsigned int score);

    /**
    \brief Advances the game to the next level.
    */
    void nextLevel();

    /**
     * \brief Checks if the player has beaten the high score and updates it if necessary.
     */
    void checkHighScore();

    /**
     * \brief Resets the scene to the initial state.
     */
    void resetScene();

    /**
     * \brief Starts the game loop and continues until the game is stopped.
     */
    void start();

    /**
     * \brief Returns whether the game is currently stopped.
     * \return True if the game is stopped, false otherwise.
     */
    const bool stop() const;

    /**
     * \brief Sets the state of all Ghost objects to the given state.
     * \param s The state to set for all Ghost objects.
     */
    void setStateGhost(const State& s);

    /**
     * \brief Sets the velocity of all Ghost objects to the given value.
     * \param v The velocity to set for all Ghost objects.
     */
    void setVelocityGhost(const float v);

    /**
     * \brief Applies the effect of a Tile on the scene.
     * \param t The Tile to apply the effect of.
     */
    void tileEffect(const Tile& t);

    /**
     * \brief Updates the positions of all entities in the scene.
     */
    void updateEntityPosition();

    /**
     * \brief Checks for collisions between entities in the scene.
     */
    void checkCollision();

    /**
     * \brief Kills the Pacman entity and decrements a life.
     */
    void die();

    /**
     * \brief Handles the eating of an entity by Pacman.
     * \param e The entity that Pacman eats.
     */
    void eatEntity(Entity* e);

    /**
     * \brief Handles the leaving of the hub by an entity.
     * \param e The entity that leaves the hub.
     */
    void leaveHub(Entity* e);
};

#endif //SCENE_HPP
