/**
 * \file pacman.hpp
 * \brief
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef PACMAN_HPP
#define PACMAN_HPP

#include "entity.hpp"
#include "direction.hpp"

/**
 * \brief Class representing the Pacman entity in the game.
 */
class Pacman : public Entity {
protected:
    int life;                   /**< The current number of lives of Pacman. */
    Direction requestDirection_; /**< The direction requested by the user for Pacman to move in. */
public:
    /**
     * \brief Default constructor for Pacman.
     */
    Pacman();

    /**
     * \brief Constructor for Pacman, with initial position.
     * \param p The initial position of Pacman.
     */
    Pacman(const Position<float>& p);

    /**
     * \brief Increases Pacman's number of lives by one.
     */
    void addLife();

    /**
     * \brief Decreases Pacman's number of lives by one.
     */
    void removeLife();

    /**
     * \brief Sets the direction requested by the user for Pacman to move in.
     * \param d The direction requested by the user.
     */
    void setRequestDirection(const Direction& d);

    /**
     * \brief Returns the direction requested by the user for Pacman to move in.
     * \return The direction requested by the user.
     */
    const Direction getRequestDirection() const;

    /**
     * \brief Sets the current number of lives of Pacman.
     * \param l The new number of lives for Pacman.
     */
    void setLife(unsigned int l);

    /**
     * \brief Returns the current number of lives of Pacman.
     * \return The current number of lives of Pacman.
     */
    int getLife() const;

    /**
     * \brief Resets Pacman's attributes to their initial values.
     */
    virtual void reset() override;

    /**
     * \brief Handles Pacman's death.
     */
    virtual void die() override;
};

#endif //PACMAN_HPP