/**
 * \file entity.hpp
 * \brief
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <iostream>
#include <vector>
#include <cmath>
#include <random>
#include "direction.hpp"
#include "position.hpp"

using std::string;
using std::vector;
using std::uniform_int_distribution;

enum class State {
    Idle,
    Chase,
    Frightened,
    FrightenedEnd,
    Dead
};

class Entity {
private:
    string name_;
    unsigned int width_;
    unsigned int height_;
    float velocity_;
    Position<float> position_;
    Direction direction_;
    State state_;
public:
    /**
     * \fn Entity()
     * \brief Deleted default constructor.
     */
    Entity() = delete;

    /**
     * \fn Entity(const string& entityName, unsigned int entityWidth, unsigned int entityHeight, float entityVelocity, Position<float> entityPosition, Direction entityDirection, State entityState)
     * \brief Constructor for the Entity class.
     * \param entityName The name of the entity.
     * \param entityWidth The width of the entity (in pixels).
     * \param entityHeight The height of the entity (in pixels).
     * \param entityVelocity The velocity of the entity (in pixels per second).
     * \param entityPosition The initial position of the entity (in Cartesian coordinates).
     * \param entityDirection The initial direction of the entity.
     * \param entityState The initial state of the entity.
     */
    Entity(const string& entityName, unsigned int entityWidth, unsigned int entityHeight, float entityVelocity, Position<float> entityPosition, Direction entityDirection, State entityState);

    /**
     * \fn ~Entity()
     * \brief Default destructor for the Entity class.
     */
    virtual ~Entity() = default;

    /**
     * \fn const string& getName() const
     * \brief Returns the name of the entity.
     * \return The name of the entity.
     */
    const string& getName() const;

    /**
     * \fn unsigned int getWidth() const
     * \brief Returns the width of the entity.
     * \return The width of the entity.
     */
    unsigned int getWidth() const;

    /**
     * \fn unsigned int getHeight() const
     * \brief Returns the height of the entity.
     * \return The height of the entity.
     */
    unsigned int getHeight() const;

    /**
     * \fn float getVelocity() const
     * \brief Returns the velocity of the entity.
     * \return The velocity of the entity.
     */
    float getVelocity() const;

    /**
     * \fn const Position<float>& getPosition() const
     * \brief Returns the position of the entity.
     * \return The position of the entity.
     */
    const Position<float>& getPosition() const;
    const Position<float> getBottomPosition() const;
    const Position<float> getTopPosition() const;
    const Position<float> getCenterPosition() const;

    /**
     * \fn const Direction& getDirection() const
     * \brief Returns the direction in which the entity is currently moving.
     * \return The direction of the entity.
     */
    const Direction& getDirection() const;

    /**
     * \fn const State& getState() const
     * \brief Returns the state of the entity.
     * \return The state of the entity.
     */
    const State& getState() const;

    /**
     * \fn void setName(const string& entityName)
     * \brief Sets the name of the entity.
     * \param entityName The new name of the entity.
     */
    void setName(const string& entityName);

    /**
     * \fn void setVelocity(float entityVelocity)
     * \brief Sets the velocity of the entity.
     * \param entityVelocity The new velocity of the entity.
     */
    void setVelocity(float entityVelocity);

    /**
     * \fn void setPosition(const Position<float>& entityPosition)
     * \brief Sets the position of the entity.
     * \param entityPosition The new position of the entity.
     */
    void setPosition(const Position<float>& entityPosition);

    /**
     * \fn void setDirection(const Direction& entityDirection)
     * \brief Sets the direction in which the entity is currently moving.
     * \param entityDirection The new direction of the entity.
     */
    inline void setDirection(const Direction& entityDirection) {direction_ = entityDirection;}

    /**
     * \fn void setState(const State& entityState)
     * \brief Sets the state of the entity.
     * \param entityState The new state of the entity.
     */
    void setState(const State& entityState);

    /**
     * \fn virtual bool checkCollision(const Entity& other) const
     * \brief Checks if the entity collides with another entity.
     * \param other The other entity to check for collision.
     * \return 'true' if the entity collides with the other entity, 'false' otherwise.
     */
    virtual bool checkCollision(const Entity& other) const;

    /**
     * \fn void handleCollision(const Entity& other)
     * \brief Handles a collision between the entity and another entity.
     * \param other The other entity with which the collision occurred.
     */
    void handleCollision(const Entity& other);

    /**
     * \fn void updatePosition(float deltaTime)
     * \brief Updates the position of the entity based on the elapsed time since the last update.
     * \param deltaTime The time elapsed since the last update (in seconds).
     */
    void updatePosition(float deltaTime);

    /**
     * \brief Sets the direction of the entity based on available directions.
     * \param pac Entity used to calculate the direction.
     * \param directions Vector of available directions.
     */
    void setDirection(const Entity& pac, const vector<Direction> directions);

    /**
     * \brief Resets the state of the entity to its initial state.
     */
    virtual void reset()=0;

/**
 * \brief Sets the state of the entity to "dead".
 */
    virtual void die()=0;
};

#endif //ENTITY_HPP
