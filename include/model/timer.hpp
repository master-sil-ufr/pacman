#ifndef PACMAN_TIMER_HPP
#define PACMAN_TIMER_HPP

class Timer{
private:
    unsigned int ghostFrightened_;
    unsigned int game_;
public:
    Timer();
    void resetTime();
    void addTime(unsigned int time);
    void removeTime(unsigned int time);
    unsigned int getTime() const;
    void resetGhostTimer();
    void increaseGhostTimer(unsigned int time);
    void decreaseGhostTimer(unsigned int time);
    unsigned int getGhostTimer() const;
};

#endif //PACMAN_TIMER_H
