/**
 * \file inky.hpp
 * \brief
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef INKY_HPP
#define INKY_HPP

#include "ghost.hpp"
#include "position.hpp"

/**
 * \brief Class representing the Inky ghost entity in the game.
 */
class Inky : public Ghost {
private:
public:
    /**
     * \brief Default constructor for Inky.
     */
    Inky();

    /**
     * \brief Constructor for Inky, with initial position.
     * \param p The initial position of Inky.
     */
    Inky(const Position<float>& p);

    /**
     * \brief Resets Inky's attributes to their initial values.
     */
    virtual void reset() override;

    /**
     * \brief Returns the time (in game ticks) that Inky should leave the ghost house.
     * \return The time (in game ticks) that Inky should leave the ghost house.
     */
    static unsigned int getTimeToleaveHub()
    {
        return 4800;
    }
};

#endif //INKY_HPP