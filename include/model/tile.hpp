/**
 * \file tile.hpp
 * \brief This file contains the definition of the Tile enumeration,
 * representing the different elements present on the grid cells of
 * the Pac-Man game.
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef TILE_HPP
#define TILE_HPP

enum class Tile{
    Wall,
    Pellet,
    PowerPellet,
    Cherry,
    Strawberry,
    Orange,
    Apple,
    Melon,
    Galaxian,
    Bell,
    Key,
    Empty
};

#endif //TILE_HPP
