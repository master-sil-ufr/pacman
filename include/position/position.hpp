/**
 * \file position.hpp
 * \brief This class represents a position in a 2D Cartesian space for
 * different data types (e.g., int and float) and provides methods for
 * manipulating and converting the coordinates.
 * \author Filipe AUGUSTO & Luc BURCKEL
 */
#ifndef POSITION_HPP
#define POSITION_HPP

#include <iostream>

template <typename T>
class Position final {
private:
    T x_; // x-coordinate of the position
    T y_; // y-coordinate of the position
public:
    /**
     * \fn Position()
     * \brief Deleted default constructor.
     */
    Position() = delete;

    /**
     * \fn Position(T xCoord, T yCoord)
     * \brief Constructor with x and y coordinates.
     * \param xCoord x-coordinate of the position.
     * \param yCoord y-coordinate of the position.
     */
    Position(T xCoord, T yCoord);

    /**
     * \fn ~Position()
     * \brief Default destructor.
     */
    ~Position() = default;

    /**
     * \fn inline T getX() const
     * \brief Returns the x-coordinate of the position.
     * \return The x-coordinate.
     */
    inline T getX() const {return x_;}

    /**
     * \fn inline T getY() const
     * \brief Returns the y-coordinate of the position.
     * \return The y-coordinate.
     */
    inline T getY() const {return y_;}

    /**
     * \fn void setX(const T xCoord)
     * \brief Modifies the x-coordinate of the position.
     * \param xCoord The new x-coordinate.
     */
    void setX(const T xCoord);

    /**
     * \fn void setY(const T yCoord)
     * \brief Modifies the y-coordinate of the position.
     * \param yCoord The new y-coordinate.
     */
    void setY(const T yCoord);

    /**
     * \fn void addX(const T deltaX)
     * \brief Adds a value to the x-coordinate of the position.
     * \param deltaX The value to add to x.
     */
    void addX(const T deltaX);

    /**
     * \fn void addY(const T deltaY)
     * \brief Adds a value to the y-coordinate of the position.
     * \param deltaY The value to add to y.
     */
    void addY(const T deltaY);

    /**
     * \fn Position<int> toInt() const
     * \brief Converts the position to Position<int>.
     * \return The position converted to Position<int>.
     */
    Position<int> toInt() const;

    /**
     * \fn Position<float> toFloat() const
     * \brief Converts the position to Position<float>.
     * \return The position converted to Position<float>.
     */
    Position<float> toFloat() const;

    /**
     * \fn Position<T>& operator=(const Position<T>& other)
     * \brief Overloads the assignment operator to copy a position.
     * \param other The position to copy.
     * \return The copied position.
     */
    Position<T>& operator=(const Position<T>& other);

    /**
     * \fn bool operator==(const Position& other) const
     * \brief Overloads the equality operator to compare two Position objects.
     * \param other The Position object to compare with the current object.
     * \return 'true' if both objects are equal, 'false' otherwise.
     */
    bool operator==(const Position<T>& other) const;
};

#include "position.tpp"

#endif //POSITION_HPP