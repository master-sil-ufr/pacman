#ifndef POSITION_TPP
#define POSITION_TPP

template <typename T>
Position<T>::Position(T xCoord, T yCoord) :
        x_{ xCoord },
        y_{ yCoord }
{}

template <typename T>
void Position<T>::setX(const T xCoord)
{
    x_ = xCoord;
}

template <typename T>
void Position<T>::setY(const T yCoord)
{
    y_ = yCoord;
}

template <typename T>
void Position<T>::addX(const T deltaX)
{
    x_ += deltaX;
}

template <typename T>
void Position<T>::addY(const T deltaY)
{
    y_ += deltaY;
}

template <typename T>
Position<int> Position<T>::toInt() const
{
    return Position<int>{static_cast<int>(x_), static_cast<int>(y_)};
}

template <typename T>
Position<float> Position<T>::toFloat() const
{
    return Position<float>{static_cast<float>(x_), static_cast<float>(y_)};
}

template <typename T>
Position<T>& Position<T>::operator=(const Position<T>& other)
{
    if (this != &other) {
        x_ = other.getX();
        y_ = other.getY();
    }

    return *this;
}

template <typename T>
bool Position<T>::operator==(const Position<T>& other) const
{
    return (x_ == other.getX()) && (y_ == other.getY());
}

#endif //POSITION_TPP
