/**
 * \file sprite.hpp
 * \brief
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef SPRITE_HPP
#define SPRITE_HPP

#include <SDL.h>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include "tile.hpp"
#include "direction.hpp"
#include "entity.hpp"
#include "pacman.hpp"

#define SCALE 3
#define LARGEUR 900
#define HAUTEUR 800

using std::vector;
using std::string;

class Sprite{
private:
    SDL_Surface* winSurf_; //winSurf
    SDL_Surface* spriteSheet_; //plancheSprites
    SDL_Rect src_; //src
    SDL_Rect bg_; //bg
    int animation_; //animation
    int counter_; //compteur
public:
    /**
     * \brief Constructor of the Sprite class.
     * \param pWindow_ A pointer to the SDL window.
     */
    Sprite(SDL_Window* pWindow_);

    /**
     * \brief Destructor of the Sprite class.
     */
    ~Sprite();

    /**
     * \brief Clears the screen.
     */
    void clear();

    /**
     * \brief Displays on the screen.
     * \param transparent A boolean value to enable/disable transparency.
     */
    void display(const bool transparent);

    /**
     * \brief Displays the high score on the screen.
     * \param score The user's high score.
     */
    void highScore(const unsigned int score);

    /**
     * \brief Displays the user's score with "1UP" on the screen.
     * \param score The user's score.
     */
    void score1UP(const unsigned int score);

    /**
     * \brief Displays the user's score with "2UP" on the screen.
     * \param score The user's score.
     */
    void score2UP(const unsigned int score);

    /**
     * \brief Displays the "Pacman" logo on the screen.
     */
    void logo();

    /**
     * \brief Displays the "Namco" logo on the screen.
     */
    void namco();

    /**
     * \brief Displays a blue terrain on the window.
     */
    void blueMap();

    /**
     * \brief Displays a white terrain on the window.
     */
    void whiteMap();

    /**
     * \brief Displays a ghost on the window.
     * \param entity The entity to display.
     * \param yCoord The y coordinate of the entity.
     */
    void ghost(const Entity& entity, int yCoord);

    /**
     * \brief Displays Pacman on the window.
     * \param pacman The Pacman entity to display.
     */
    void pacman(const Pacman& pacman);

    /**
     * \brief Displays Blinky on the window.
     * \param ent The Blinky entity to display.
     */
    void blinky(const Entity& ent);

    /**
     * \brief Displays Clyde on the window.
     * \param ent The Clyde entity to display.
     */
    void clyde(const Entity& ent);

    /**
     * \brief Displays Inky on the window.
     * \param ent The Inky entity to display.
     */
    void inky(const Entity& ent);

    /**
     * \brief Displays Pinky on the window.
     * \param ent The Pinky entity to display.
     */
    void pinky(const Entity& ent);

    /**
     * \brief Displays Pacman's idle animation on the window.
     * \param xCoord The x coordinate to display Pacman's idle.
     * \param yCoord The y coordinate to display Pacman's idle.
     */
    void pacmanIDLE(const int xCoord, const int yCoord);

    /**
     * \brief Displays Blinky's idle animation on the window.
     * \param xCoord The x coordinate to display Blinky's idle.
     * \param yCoord The y coordinate to display Blinky's idle.
     */
    void blinkyIDLE(const int xCoord, const int yCoord);

    /**
     * \brief Displays Clyde's idle animation on the window.
     * \param xCoord The x coordinate to display Clyde's idle.
     * \param yCoord The y coordinate to display Clyde's idle.
     */
    void clydeIDLE(const int xCoord, const int yCoord);

    /**
     * \brief Displays Inky's idle animation on the window.
     * \param xCoord The x coordinate to display Inky's idle.
     * \param yCoord The y coordinate to display Inky's idle.
     */
    void inkyIDLE(const int xCoord, const int yCoord);

    /**
     * \brief Displays Inky's idle animation on the window.
     * \param xCoord The x coordinate to display Pinky's idle.
     * \param yCoord The y coordinate to display Pinky's idle.
     */
    void pinkyIDLE(const int xCoord, const int yCoord);

    /**
     * \brief Displays all the items on the game grid on the window.
     * \param grid The game grid containing all the items to display.
     */
    void item(const vector<vector<Tile>> grid);

    /**
     * \brief Displays a Pac-Dot item on the window.
     * \param xCoord The x-coordinate of the Pac-Dot's position.
     * \param yCoord The y-coordinate of the Pac-Dot's position.
     */
    void pacDot(const int xCoord, const int yCoord);

    /**
     * \brief Displays a Power Pellet item on the window.
     * \param xCoord The x-coordinate of the Power Pellet's position.
     * \param yCoord The y-coordinate of the Power Pellet's position.
     */
    void powerPellet(const int xCoord, const int yCoord);

    /**
     * \brief Adds a text string to the window.
     * \param text The text string to add.
     * \param xCoord The x-coordinate of the text's position.
     * \param yCoord The y-coordinate of the text's position.
     * \param color The color of the text (default is white).
     */
    void addText(string text, int xCoord, const int yCoord, string color="white");
};

#endif //SPRITE_HPP