/**
 * \file window.hpp
 * \brief This file defines the Window class, used to create a SDL window
 * and display different elements on it.
 * \author Filipe AUGUSTO & Luc BURCKEL
 */

#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <iostream>
#include <cstring>
#include <SDL.h>
#include <memory>
#include "sprite.hpp"
#include "map.hpp"
#include "scene.hpp"

/**
 * \class Window
 * \brief This class is used to create a SDL window and display different elements on it.
 */
class Window {
private:
    SDL_Window* pWindow_;
    Sprite* sprite_;
public:
    /**
     * \fn
     * \brief Constructor for the Window class.
     */
    Window();

    /**
     * \fn
     * \brief Destructor for the Window class.
     */
    ~Window();

    /**
     * \fn
     * \brief Displays the main menu on the window.
     * \param choice The user's choice (1 for "play", 2 for "quit").
     * \param highScore The user's high score.
     */
    void showMenu(unsigned int choice, unsigned int highScore) const;

    /**
     * \fn
     * \brief Displays the terrain on the window.
     * \param scene The Scene object containing the terrain to be displayed.
     */
    void showMap(const Scene& scene) const;

    /**
     * \fn
     * \brief Starts the animation of the Sprite object.
     */
    void startAnimation() const;

    /**
     * \fn
     * \brief Displays the win animation on the window.
     */
    void winAnimation() const;
};

#endif //WINDOW_HPP
