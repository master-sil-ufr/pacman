#include "window.hpp"

Window::Window()
{
    pWindow_ = SDL_CreateWindow("PacMan", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, LARGEUR, HAUTEUR, SDL_WINDOW_SHOWN);
    sprite_ = new Sprite(pWindow_);
}

Window::~Window()
{
    SDL_DestroyWindow(pWindow_);
}

void Window::showMenu(unsigned int choice, unsigned int highScore) const
{
    sprite_->clear();
    sprite_->highScore(highScore);
    sprite_->logo();

    if (choice == 1) {
        sprite_->addText("> play", 345, 430);
        sprite_->addText("  quit", 345, 470);
    } else{
        sprite_->addText("  play", 345, 430);
        sprite_->addText("> quit", 345, 470);
    }

    sprite_->namco();
    sprite_->addText("tm $ 1980 1993 namco ltd.", 119, 650);
    sprite_->addText("namco hometek, inc.", 214, 700);
    sprite_->addText("licensed by luc and filipe", 130, 750);

    SDL_UpdateWindowSurface(pWindow_);
}

void Window::showMap(const Scene& scene) const
{
    sprite_->clear();

    sprite_->highScore(scene.getHighScore());
    sprite_->score1UP(scene.getScore());
    sprite_->blueMap();
    sprite_->item(scene.getMap().getGrid());
    sprite_->pacman(*dynamic_cast<Pacman*>(scene.getEntity("pacman")));
    sprite_->blinky(*scene.getEntity("blinky"));
    sprite_->clyde(*scene.getEntity("clyde"));
    sprite_->inky(*scene.getEntity("inky"));
    sprite_->pinky(*scene.getEntity("pinky"));

    SDL_UpdateWindowSurface(pWindow_);
}

void Window::startAnimation() const
{
    sprite_->clear();
    sprite_->score1UP(0);
    sprite_->score2UP(0);
    sprite_->highScore(0);
    sprite_->addText("character / nickname",250, 100);
    sprite_->addText("credit   0",100, 750);
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(1000);

    sprite_->blinkyIDLE(160, 145);
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(1000);

    sprite_->addText("-shadow", 250, 165,"red");
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(500);

    sprite_->addText("=blinky=", 538, 165,"red");
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(500);

    sprite_->pinkyIDLE(160,225);
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(1000);

    sprite_->addText("-speedy", 250, 240,"pink");
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(500);

    sprite_->addText("=pinky=", 538, 240,"pink");
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(500);

    sprite_->inkyIDLE(160,305);
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(1000);

    sprite_->addText("-bashful", 250, 315,"blue");
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(500);

    sprite_->addText("=inky=", 538, 315,"blue");
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(500);

    sprite_->clydeIDLE(160, 385);
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(1000);

    sprite_->addText("-pokey", 250, 390,"orang");
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(500);

    sprite_->addText("=clyde=", 538, 390,"orang");
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(1000);

    sprite_->pacDot(300, 600);
    sprite_->powerPellet(300, 650);
    sprite_->addText("10 pts", 360, 600);
    sprite_->addText("50 pts", 360, 650);
    SDL_UpdateWindowSurface(pWindow_);
    SDL_Delay(1000);
}

void Window::winAnimation() const
{
    SDL_Delay(700);

    for (int i = 0; i < 4; i++)
    {
        sprite_->whiteMap();
        SDL_Delay(300);
        SDL_UpdateWindowSurface(pWindow_);

        sprite_->blueMap();
        SDL_Delay(300);
        SDL_UpdateWindowSurface(pWindow_);
    }
}