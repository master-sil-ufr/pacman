#include "sprite.hpp"

Sprite::Sprite(SDL_Window* pWindow_):
        animation_{1},
        counter_{0}
{
    winSurf_ = SDL_GetWindowSurface(pWindow_);
    spriteSheet_ = SDL_LoadBMP("./pacman_sprites.bmp");
}

Sprite::~Sprite()
{
    delete winSurf_;
    delete spriteSheet_;
}

void Sprite::display(const bool transparent)
{
    SDL_SetColorKey(spriteSheet_, transparent, 0);
    SDL_BlitScaled(spriteSheet_, &bg_, winSurf_, &src_);
}

void Sprite::clear()
{
    src_ = { 0,0, LARGEUR,HAUTEUR };
    bg_ = { 0,0, 1,1 };
    counter_++;

    if (counter_ >= 7) {
        counter_ = 0;
        animation_ = (animation_ + 1) % 2;
    }

    display(false);
}

void Sprite::highScore(const unsigned int score)
{
    char scoreChar[8];
    int positionX = 345;

    addText("high score",positionX,20);

    if (score != 0) {
        snprintf(scoreChar, sizeof(scoreChar), "%d", score);
        positionX += (10 - static_cast<int>(strlen(scoreChar))) * 21;
        addText(scoreChar, positionX, 50);
    }
}

void Sprite::score1UP(const unsigned int score)
{
    char scoreChar[8];
    int positionX=172;

    if (animation_ == 0) {
        addText("1up", positionX, 20);
    } if (score != 0) {
        snprintf(scoreChar, sizeof(scoreChar), "%d", score);
        positionX += (4 - static_cast<int>(strlen(scoreChar))) * 21;
        addText(scoreChar, positionX, 50);
    }
}

void Sprite::score2UP(const unsigned int score)
{
    char scoreChar[8];
    int positionX = 686;

    if (animation_ == 0) {
        addText("2up", positionX, 20);
    } if (score != 0) {
        snprintf(scoreChar, sizeof(scoreChar), "%d", score);
        positionX += (10 - static_cast<int>(strlen(scoreChar))) * 21;
        addText(scoreChar, positionX, 50);
    }
}

void Sprite::logo()
{
    bg_ = { 4,4, 180,46 };
    src_ = { (LARGEUR-bg_.w*SCALE)/2,130, bg_.w*SCALE,bg_.h*SCALE };

    display(true);
}

void Sprite::namco()
{
    bg_ = { 28,78, 60,8 };
    src_ = { (LARGEUR-bg_.w*SCALE)/2,600, bg_.w*SCALE,bg_.h*SCALE };

    display(true);
}

void Sprite::blueMap(){
    bg_ = { 369,3, 168,216 };
    src_ = { 201,90, bg_.w*SCALE,bg_.h*SCALE };
    display(true);
}

void Sprite::whiteMap(){
    bg_ = { 540,3, 168,216 };
    src_ = { 201,90, bg_.w*SCALE,bg_.h*SCALE };
    display(true);
}

void Sprite::ghost(const Entity& entity, int yCoord)
{
    switch(entity.getState()) {
        case State::Chase :
            switch (entity.getDirection()) {
                case Direction::Left:
                    bg_ = {38 + 17 * animation_, yCoord, 14, 14};
                    break;
                case Direction::Right:
                    bg_ = {4 + 17 * animation_, yCoord, 14, 14};
                    break;
                case Direction::Down:
                    bg_ = {106 + 17 * animation_, yCoord, 14, 14};
                    break;
                case Direction::Up:
                    bg_ = {72 + 17 * animation_, yCoord, 14, 14};
                    break;
                default:
                    bg_ = {4, yCoord, 14, 14};
                    break;
            }
            break;
        case State::Idle:
            bg_ = {4, yCoord, 14, 14};
            break;
        case State::Frightened :
            bg_ = {4 + 17 * animation_, 196, 14, 14};
            break;
        case State::Dead :
            switch (entity.getDirection()) {
                case Direction::Left:
                    bg_ = {90, 197, 14, 14};
                    break;
                case Direction::Right:
                    bg_ = {72, 197, 14, 14};
                    break;
                case Direction::Down:
                    bg_ = {123, 197, 14, 14};
                    break;
                case Direction::Up:
                    bg_ = {106, 197, 14, 14};
                    break;
                default:
                    bg_ = {106, 197, 14, 14};
                    break;
            }
            break;
        case State::FrightenedEnd:
            bg_ = {4 + 34 * animation_, 196, 14, 14};
            break;
        default:
            break;
    }

    src_.x = 201 + entity.getPosition().getX();
    src_.y = 90 + entity.getPosition().getY();
    src_.w = entity.getWidth();
    src_.h = entity.getHeight();

    display(false);
}

void Sprite::pacman(const Pacman& pacman)
{
    switch (pacman.getDirection()) {
        case Direction::Left:
            bg_ = { 48+13*animation_,90, 14,14 };
            break;
        case Direction::Right:
            bg_ = { 20+14*animation_,90, 14,14 };
            break;
        case Direction::Down:
            bg_ = { 110+17*animation_,90, 14,14 };
            break;
        case Direction::Up:
            bg_ = { 76+17*animation_,90, 14,14 };
            break;
        default:
            bg_ = { 4,90, 14,14 };
            break;
    }
    src_.x = 201 + pacman.getPosition().getX();
    src_.y = 90 + pacman.getPosition().getY();
    src_.w = pacman.getWidth();
    src_.h = pacman.getHeight();

    display(true);

    for (int i = 0 ; i < pacman.getLife(); i++) {
        pacmanIDLE(250 + (i * 40), 755);
    }
}

void Sprite::blinky(const Entity& entity)
{
    ghost(entity, 124);
}

void Sprite::clyde(const Entity& entity)
{
    ghost(entity, 178);
}

void Sprite::inky(const Entity& entity)
{
    ghost(entity, 160);
}

void Sprite::pinky(const Entity& entity)
{
    ghost(entity, 142);
}

void Sprite::pacmanIDLE(const int xCoord,const int yCoord)
{
    bg_ = { 169,76, 12,12 };
    src_ = { xCoord,yCoord, 30,30 };

    display(true);
}

void Sprite::blinkyIDLE(const int xCoord, const int yCoord)
{
    bg_ = { 4,124, 14,14 };
    src_ = { xCoord,yCoord, 50,50 };

    display(true);
}

void Sprite::clydeIDLE(const int xCoord,const int yCoord)
{
    bg_ = { 4,178, 14,14 };
    src_ = { xCoord,yCoord, 50,50 };

    display(true);
}

void Sprite::inkyIDLE(const int xCoord,const int yCoord)
{
    bg_ = { 4,160, 14,14 };
    src_ = { xCoord,yCoord, 50,50 };

    display(true);
}

void Sprite::pinkyIDLE(const int xCoord,const int yCoord)
{
    bg_ = { 4,142, 14,14 };
    src_ = { xCoord,yCoord, 50,50 };

    display(true);
}

void Sprite::item(const vector<vector<Tile>> grid)
{
    for (int i = 0; i < grid.size(); i++) {
        for (int y = 0; y < grid[i].size(); y++){
            switch (grid[i][y]) {
                case Tile::PowerPellet:
                    if (animation_ == 0) {
                        bg_ = {9, 79, 7, 7};
                        src_ = {201 + y * 8 * SCALE, 90 + i * 8 * SCALE, 8 * SCALE, 8 * SCALE};

                        display(true);
                    }
                    break;
                case Tile::Pellet:
                    bg_ ={ 1,78, 8,8 };
                    src_ = { 201+y*8*SCALE,90+i*8*SCALE, bg_.w*SCALE,bg_.h*SCALE };

                    display(true);
                    break;
                default:
                    break;
            }
        }
    }
}

void Sprite::pacDot(const int xCoord,const int yCoord)
{
    bg_ = { 1,78, 8,8 };
    src_ = {xCoord, yCoord, 8 * SCALE, 8 * SCALE};

    display(true);
}

void Sprite::powerPellet(const int xCoord,const int yCoord)
{
    if (animation_ == 0) {
        bg_ = {9, 79, 7, 7};
        src_ = {xCoord, yCoord, 8 * SCALE, 8 * SCALE};

        display(true);
    }
}

void Sprite::addText(string text, int xCoord, const int yCoord, string color)
{
    const int size = 8;

    for (char i = 0; i < text.length(); i++) {
        const int ascii = text[i];

        if (ascii >= 97 && ascii <= 111) {
            bg_ = { 12+(ascii-97)*8,61, size,size };
        } else if (ascii > 111 && ascii <= 123) {
            bg_ = { 4+(ascii-112)*size,69, size,size };
        } else if (ascii >= 48 && ascii <= 57) {
            bg_ = { 4+(ascii-48)*size,53, size,size };
        } else {
            switch (text[i]) {
                case '.':
                    bg_ ={ 91,69, size,size };
                    break;
                case '>':
                    bg_ ={ 100,69, size,size };
                    break;
                case '$':
                    bg_ ={ 116,69, size,size };
                case '-':
                    bg_ ={ 84,53, size,size };
                    break;
                case '/':
                    bg_ ={ 92,53, size,size };
                    break;
                case '!':
                    bg_ ={ 100,53, size,size };
                    break;
                case ',':
                    bg_ ={ 109,50, size,size };
                    break;
                default:
                    bg_ ={ 0,0, 0,0 };
                    break;
            }
        }
        src_ = { xCoord+((size+1)*SCALE)*i,yCoord, size*SCALE,size*SCALE };
        display(false);
    }
}