#include "map.hpp"

Map::Map(unsigned int tileSize):
        tileSize_{  tileSize }
{
    this->loadMap();

    gridWidth_ = grid_.size();
    gridHeight_ = grid_.empty() ? 0 : grid_[0].size();
}

unsigned int Map::getNumberPellet() const
{
    return numberPellet_;
}

unsigned int Map::getNumberPower() const
{
    return numberPellet_;
}

Tile Map::getTileValue(const Position<int>& tilePosition) const
{
    int row = tilePosition.getY();
    int column = tilePosition.getX();

    if((row >= gridWidth_) || (row < 0)) {
        throw out_of_range("Row not in range.");
    } if((column >= gridHeight_) || (column < 0)) {
        throw out_of_range("Column not in range.");
    }

    return grid_[row][column];
}

Tile Map::getNextTileValue(const Direction& objectDirection, const Position<int>& tilePosition) const
{
    Position nextTilePosition = tilePosition;

    switch (objectDirection) {
        case Direction::Right:
            nextTilePosition.addX(1);
            break;
        case Direction::Left:
            nextTilePosition.addX(-1);
            break;
        case Direction::Up:
            nextTilePosition.addY(-1);
            break;
        case Direction::Down:
            nextTilePosition.addY(1);
            break;
        default:
            break;
    }

    return this->getTileValue(nextTilePosition);
}

const vector<vector<Tile>> &Map::getGrid() const
{
    return grid_;
}

unsigned int Map::countTileInTheGrid(const Tile& tileValue) const
{
    unsigned int numberTile = 0;

    for (const auto& row : grid_) {
        numberTile += std::count(row.begin(), row.end(), tileValue);
    }

    return numberTile;
}

unsigned int Map::countPelletInTheGrid() const
{
    return this->countTileInTheGrid(Tile::Pellet);
}

unsigned int Map::countPowerPelletInTheGrid() const
{
    return this->countTileInTheGrid(Tile::PowerPellet);
}

void Map::setTileValue(const Position<int>& tilePosition, const Tile& newValue)
{
    int row = tilePosition.getY();
    int column = tilePosition.getX();

    if((row >= gridWidth_) || (row < 0)) {
        throw out_of_range("Row not in range.");
    } if((column >= gridHeight_) || (column < 0)) {
        throw out_of_range("Column not in range.");
    }

    grid_[row][column] = newValue;
}

void Map::setFruit()
{
    //
}

void Map::loadMap()
{
    grid_.clear();
    std::string line;
    std::ifstream map("grid.txt");

    if (!map.is_open()) {
        std::cerr << "Error: file can't be open" << std::endl;
        return;
    }

    while (getline(map, line)) {
        parseMap(line);
    }

    map.close();
}

void Map::parseMap(string& line)
{
    std::vector<Tile> row;

    for (char i : line) {
        switch (i) {
            case 'w':
                row.push_back(Tile::Wall);
                break;
            case 'c':
                row.push_back(Tile::Pellet);
                numberPellet_++;
                break;
            case 'p':
                row.push_back(Tile::PowerPellet);
                numberPowerPellet_++;
                break;
            default:
                row.push_back(Tile::Empty);
                break;
        }
    }

    for (int i = static_cast<int>(line.length()) - 2; i >= 0; --i) {
        switch (line[i]) {
            case 'w':
                row.push_back(Tile::Wall);
                break;
            case 'c':
                row.push_back(Tile::Pellet);
                numberPellet_++;
                break;
            case 'p':
                row.push_back(Tile::PowerPellet);
                numberPowerPellet_++;
                break;
            default:
                row.push_back(Tile::Empty);
                break;
        }
    }

    grid_.push_back(row);
}

Position<int> Map::mapPixelPositionToMapPosition(const Position<int>& pixelPosition) const
{
    return Position<int>{
            static_cast<int>(std::floor(pixelPosition.getX() / tileSize_)),
            static_cast<int>(std::floor(pixelPosition.getY() / tileSize_))
    };
}

Position<int> Map::discretizePosition(const Position<float>& continuousPosition) const
{
    return continuousPosition.toInt();
}

Position<float> Map::calculateObjectCenterPosition(const Position<float>& continuousPosition, const unsigned int width, const unsigned int height) const
{
    return Position<float>{
            continuousPosition.getX() + static_cast<float>(width) / 2.0f,
            continuousPosition.getY() + static_cast<float>(height) / 2.0f
    };
}

Tile Map::consumeTile(const Position<int>& tilePosition)
{
    Tile tileValue = this->getTileValue(tilePosition);

    if (tileValue == Tile::Wall) {
        std::cerr << "Error: Trying to eat a wall." << std::endl;
        exit(EXIT_FAILURE);
    } if (tileValue != Tile::Empty) {
        if(tileValue==Tile::Pellet) {
            numberPellet_--;
        } if(tileValue==Tile::PowerPellet) {
            numberPowerPellet_--;
        }

        this->setTileValue(tilePosition, Tile::Empty);
    }

    return tileValue;
}

Position<float> Map::getPositionOnMap(const Entity& entity) const
{
    return Position<float>{
            entity.getCenterPosition().getX() / static_cast<float>(tileSize_),
            entity.getCenterPosition().getY() / static_cast<float>(tileSize_)
    };
}

vector<Direction> Map::allDirectionCanBeChoose(const Position<int>& position, const Direction& direction) const
{
    vector<Direction> res;

    if(canMoveForward(position,Direction::Left) && direction!=Direction::Right) {
        res.push_back(Direction::Left);
    } if(canMoveForward(position,Direction::Right) && direction!=Direction::Left) {
        res.push_back(Direction::Right);
    } if(canMoveForward(position,Direction::Up) && direction!=Direction::Down) {
        res.push_back(Direction::Up);
    } if(canMoveForward(position,Direction::Down) && direction!=Direction::Up) {
        res.push_back(Direction::Down);
    }

    return res;
}

bool Map::areAllPelletsCollected() const
{
    return numberPellet_ == 0 && numberPowerPellet_ == 0;
}

bool Map::canChangeDirection(const Position<int>& topPosition, const Position<int>& bottomPosition, const Direction& direction) const
{
    Position<int> pos1 = mapPixelPositionToMapPosition(topPosition);
    Position<int> pos2 = mapPixelPositionToMapPosition(bottomPosition);

    switch (direction) {
        case Direction::Right:
            pos1.addX(1);
            pos2.addX(1);
            break;
        case Direction::Left:
            pos1.addX(-1);
            pos2.addX(-1);
            break;
        case Direction::Up:
            pos1.addY(-1);
            pos2.addY(-1);
            break;
        case Direction::Down:
            pos1.addY(1);
            pos2.addY(1);
            break;
        default:
            break;
    }

    return (getTileValue(pos1) != Tile::Wall) && (getTileValue(pos2)!=Tile::Wall);
}

bool Map::canMoveForward(const Position<int>& bottomPosition, const Direction& direction) const
{
    Position<int> pos1= mapPixelPositionToMapPosition(bottomPosition);

    switch (direction) {
        case Direction::Right:
            pos1.addX(1);
            break;
        case Direction::Left:
            pos1.addX(-1);
            break;
        case Direction::Up:
            pos1.addY(-1);
            break;
        case Direction::Down:
            pos1.addY(1);
            break;
        default:
            break;
    }

    return getTileValue(pos1) != Tile::Wall;
}