#include "timer.hpp"

Timer::Timer() : ghostFrightened_(0), game_(0) {}

void Timer::resetTime()
{
    game_=0;
}
void Timer::addTime(unsigned int time)
{
    game_+=time;
}
void Timer::removeTime(unsigned int time)
{
    game_-=time;
}
unsigned int Timer::getTime() const
{
    return game_;
}
void Timer::resetGhostTimer()
{
    ghostFrightened_=0;
}
void Timer::increaseGhostTimer(unsigned int time)
{
    ghostFrightened_+=time;
}
void Timer::decreaseGhostTimer(unsigned int time)
{
    ghostFrightened_-=time;
}
unsigned int Timer::getGhostTimer() const
{
    return ghostFrightened_;
}
