#include "blinky.hpp"

Blinky::Blinky() :
        Ghost("blinky",{10*24,10*24},State::Chase,Direction::Left)
{}

Blinky::Blinky(const Position<float>& p) :
        Ghost("blinky",p)
{}

void Blinky::reset()
{
    setDirection(Direction::Left);
    setPosition({10*24,10*24});
    setState(State::Chase);
}