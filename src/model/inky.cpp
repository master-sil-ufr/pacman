#include "inky.hpp"

Inky::Inky() :
        Ghost("inky",{9*24,12*24},State::Idle,Direction::None)
{}

Inky::Inky(const Position<float>& p) :
        Ghost("inky",p)
{}

void Inky::reset()
{
    setDirection(Direction::None);
    setPosition({9*24,12*24});
    setState(State::Idle);
}