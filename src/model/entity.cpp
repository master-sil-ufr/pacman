#include "entity.hpp"

Entity::Entity(const string& entityName, unsigned int entityWidth, unsigned int entityHeight, float entityVelocity, Position<float> entityPosition, Direction entityDirection, State entityState):
        name_{ entityName },
        velocity_{ entityVelocity },
        position_{ entityPosition },
        direction_{ entityDirection },
        state_{ entityState }
{
    if (entityWidth < 1 || entityHeight < 1) {
        std::cerr << "Error: entity size must be greater than or equal to 1" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    width_ = entityWidth;
    height_ = entityHeight;
}

const string& Entity::getName() const
{
    return name_;
}

unsigned int Entity::getWidth() const
{
    return width_;
}

unsigned int Entity::getHeight() const
{
    return height_;
}

float Entity::getVelocity() const
{
    return velocity_;
}

const Position<float>& Entity::getPosition() const
{
    return position_;
}

const Position<float> Entity::getBottomPosition() const{
    Position p=position_;
    switch (direction_) {
        case Direction::Left:
            p.addX(width_-1);
            break;
        case Direction::Up:
            p.addY(height_-1);
            break;
    }
    return p;
}

const Position<float> Entity::getTopPosition() const{
    Position p=position_;
    switch (direction_) {
        case Direction::Right:
            p.addX(width_-1);
            break;
        case Direction::Down:
            p.addY(height_-1);
            break;
    }
    return p;
}

const Position<float> Entity::getCenterPosition() const{
    Position p=position_;

    p.addX(width_/2);
    p.addY(height_/2);

    return p;
}


const Direction& Entity::getDirection() const
{
    return direction_;
}

const State& Entity::getState() const
{
    return state_;
}

void Entity::setName(const string& entityName)
{
    name_ = entityName;
}

void Entity::setVelocity(float entityVelocity)
{
    velocity_ = entityVelocity;
}

void Entity::setPosition(const Position<float>& entityPosition)
{
    position_ = entityPosition;
}

void Entity::setState(const State& entityState)
{
    state_ = entityState;
}

bool Entity::checkCollision(const Entity& other) const
{
    return ((position_.getX() <= (other.getPosition().getX() + static_cast<float>(other.getWidth()))) &&
            ((position_.getX() + static_cast<float>(width_)) >= other.getPosition().getX()) &&
            (position_.getY() <= (other.getPosition().getY() + static_cast<float>(other.getHeight()))) &&
            ((position_.getY() + static_cast<float>(height_)) >= other.getPosition().getY())
    );
}

void Entity::handleCollision(const Entity& other)
{
    if (checkCollision(other)) {
        switch (state_) {
            case State::Idle:
                //
                break;
            case State::Chase:
                //
                break;
            case State::Frightened:
                //
                break;
            case State::Dead:
                //
                break;
        }
    }
}

void Entity::updatePosition(float deltaTime)
{
    float distance = velocity_ * deltaTime;

    switch(direction_) {
        case Direction::Left:
            position_.addX(-distance);
            break;
        case Direction::Right:
            position_.addX(distance);
            break;
        case Direction::Up:
            position_.addY(-distance);
            break;
        case Direction::Down:
            position_.addY(distance);
            break;
        default:
            break;
    }
    if(getTopPosition().getX()<24.f){
        position_.setX(456.f);
    }
    if(getTopPosition().getX()>479.f){
        position_.setX(24.f);
    }
}

void Entity::setDirection(const Entity& pac,const vector<Direction> directions)
{
    Direction d=getDirection();
    std::random_device rd;  // Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd());
    uniform_int_distribution<int> distribution(0,directions.size()-1);

    d=directions.at(distribution(gen));

    direction_=d;
}
