#include "pinky.hpp"

Pinky::Pinky() :
        Ghost("pinky",{10*24,12*24},State::Idle,Direction::None)
{}

Pinky::Pinky(const Position<float>& p) :
        Ghost("pinky",p)
{}

void Pinky::reset()
{
    setDirection(Direction::None);
    setPosition({10*24,12*24});
    setState(State::Idle);
}