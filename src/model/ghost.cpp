#include "ghost.hpp"

Ghost::Ghost(string name,const Position<float>& p) :
        Entity(name, 24, 24, 3.f, p, Direction::None, State::Idle)
{}

Ghost::Ghost(string name,const Position<float>& p,const State& s,const Direction& d) :
        Entity(name, 24, 24, 3.f, p, d, s)
{}

void Ghost::die()
{
    setState(State::Chase);
    setPosition({10*24,10*24});
    setVelocity(3.f);
}