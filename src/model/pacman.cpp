#include "pacman.hpp"

Pacman::Pacman() :
    Entity("pacman", 24, 24, 3.f, {10*24,20*24}, Direction::None, State::Idle),
    life(2)
{}

Pacman::Pacman(const Position<float>& p) :
    Entity("pacman", 24, 24, 3.f, p, Direction::None, State::Chase),
    life(2)
{}

void Pacman::addLife()
{
    ++life;
}
void Pacman::removeLife()
{
    --life;
}

void Pacman::setRequestDirection(const Direction& d)
{
    requestDirection_=d;
}

const Direction Pacman::getRequestDirection() const
{
    return requestDirection_;
}

void Pacman::setLife(unsigned int l)
{
    life=l;
}

int Pacman::getLife() const
{
    return life;
}

void Pacman::reset()
{
    setDirection(Direction::None);
    setRequestDirection(Direction::None);
    setPosition({10*24,20*24});
    setState(State::Idle);
}

void Pacman::die()
{
    setDirection(Direction::None);
    setRequestDirection(Direction::None);
    setPosition({10*24,20*24});
    setState(State::Idle);
}