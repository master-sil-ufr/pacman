#include "clyde.hpp"

Clyde::Clyde() :
        Ghost("clyde",{11*24,12*24},State::Idle,Direction::None)
{}

Clyde::Clyde(const Position<float>& p) :
        Ghost("clyde",p)
{}

void Clyde::reset()
{
    setDirection(Direction::None);
    setPosition({11*24,12*24});
    setState(State::Idle);
}