//
// Created by Filipe Augusto on 22/04/2023.
//

#include "scene.hpp"

Scene::Scene():
    level_{ 0 },
    score_{ 0 },
    highScore_{ 0 },
    timer_(make_unique<Timer>()),
    gameOver_{ false },
    state_{State::Chase},
    map_(make_unique<Map>(24))
{
    entities.push_back(new Pacman());
    entities.push_back(new Blinky());
    entities.push_back(new Pinky());
    entities.push_back(new Clyde());
    entities.push_back(new Inky());
}

unsigned int Scene::getLevel() const
{
    return level_;
}

unsigned int Scene::getScore() const
{
    return score_;
}

unsigned int Scene::getHighScore() const
{
    return highScore_;
}

const vector<Entity*> &Scene::getEntities() const
{
    return entities;
}

const Map& Scene::getMap() const
{
    return *map_;
}

Entity* Scene::getEntity(string nameEntity) const
{
    for (auto e : entities) {
        if (e->getName() == nameEntity) {
            return e;
        }
    }
    return NULL;
}

void Scene::setLevel(unsigned int level) {
    level_ = level;
}

void Scene::setScore(unsigned int score) {
    score_ = score;
}

void Scene::setHighScore(unsigned int highScore) {
    highScore_ = highScore;
}

void Scene::setEntities(const vector<Entity*> &entities) {
    Scene::entities = entities;
}

void Scene::addScore(unsigned int score) {
    score_ += score;
    checkHighScore();
}

void Scene::nextLevel() {
    level_++;
    map_->loadMap();
    resetScene();
}

void Scene::checkHighScore()
{
    if(highScore_<score_)
        highScore_=score_;
}

void Scene::resetScene()
{
    timer_->resetTime();
    for(auto e : entities)
        e->reset();
}

void Scene::start()
{
    gameOver_=false;
}

const bool Scene::stop() const
{
    return gameOver_;
}

void Scene::setStateGhost(const State& s)
{
    state_=s;
    for(auto e : entities)
        if(e->getName()!="pacman")
            e->setState(s);
}

void Scene::setVelocityGhost(const float v)
{
    for(auto e : entities)
        if(e->getName()!="pacman")
            e->setVelocity(v);
}

void Scene::tileEffect(const Tile& t)
{
    switch(t){
        case Tile::PowerPellet:
            setStateGhost(State::Frightened);
            break;
    }

    std::unordered_map<Tile, unsigned int> points {
            {Tile::Pellet, 10},
            {Tile::PowerPellet, 50},
            {Tile::Cherry, 100},
            {Tile::Strawberry, 300},
            {Tile::Orange, 500},
            {Tile::Apple, 700},
            {Tile::Melon, 1000},
            {Tile::Galaxian, 2000},
            {Tile::Bell, 3000},
            {Tile::Key, 5000}
    };
    auto foundTile = points.find(t);
    if (foundTile != points.end()) {
        addScore(foundTile->second);
    }
}

void Scene::updateEntityPosition()
{
    timer_->addTime(16);
    if(state_==State::Frightened || state_==State::FrightenedEnd)
        timer_->increaseGhostTimer(16);
    for(auto e : entities){
        if(e->getName()=="pacman"){
            if(map_->canChangeDirection(e->getTopPosition().toInt(),e->getBottomPosition().toInt(),dynamic_cast<Pacman*>(e)->getRequestDirection()))
                e->setDirection(dynamic_cast<Pacman*>(e)->getRequestDirection());
            tileEffect(map_->consumeTile(map_->getPositionOnMap(*e).toInt()));
        }
        if(map_->canMoveForward(e->getBottomPosition().toInt(),e->getDirection())) {
            e->updatePosition(1);
        }
        if(e->getName()!="pacman" && (map_->mapPixelPositionToMapPosition(e->getTopPosition().toInt())==map_->mapPixelPositionToMapPosition(e->getBottomPosition().toInt()))){
            e->setDirection(*getEntity("pacman"),map_->allDirectionCanBeChoose(e->getPosition().toInt(),e->getDirection()));
            if(e->getState()==State::Frightened || e->getState()==State::FrightenedEnd)
                e->setVelocity(2);
            else
                e->setVelocity(3);

        }

    }

    if(timer_->getGhostTimer()>=2000){
        setStateGhost(State::FrightenedEnd);
    }

    if(timer_->getGhostTimer()>=3000){
        timer_->resetGhostTimer();
        setStateGhost(State::Chase);
    }

    if(Pinky::getTimeToleaveHub()==timer_->getTime())
        leaveHub(getEntity("pinky"));
    if(Clyde::getTimeToleaveHub()==timer_->getTime())
        leaveHub(getEntity("clyde"));
    if(Inky::getTimeToleaveHub()==timer_->getTime())
        leaveHub(getEntity("inky"));
}

void Scene::checkCollision()
{
    for(auto e : entities)
        if(e->getName()!="pacman")
            if(e->checkCollision(*getEntity("pacman")))
                if(e->getState()!=State::Frightened && e->getState()!=State::FrightenedEnd)
                    die();
                else
                    eatEntity(e);
}

void Scene::die()
{
    Pacman* pac=dynamic_cast<Pacman*>(getEntity("pacman"));
    if(pac->getLife()==0){
        gameOver_=true;
        score_=0;
        pac->setLife(2);
    }
    resetScene();
    pac->removeLife();
}

void Scene::eatEntity(Entity* e)
{
    addScore(200);
    e->die();
}

void Scene::leaveHub(Entity* e)
{
    e->setPosition({10*24,10*24});
    e->setState(State::Chase);
    e->setDirection(Direction::Left);
}


