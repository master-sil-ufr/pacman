#include "sdlManager.hpp"

SDLManager::SDLManager()
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0 )
    {
        std::cerr <<"Failed to initialize SDL "<<SDL_GetError() << std::endl;
    }
}

SDLManager::~SDLManager()
{
    SDL_Quit();
}

bool SDLManager::menu(bool *quit, int *choice) const
{
    int nbk;
    const Uint8* keys = SDL_GetKeyboardState(&nbk);
    SDL_Event event;

    while (!*quit && SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                *quit = true;
                break;
            default: break;
        }
    }

    // Keyboard
    if (keys[SDL_SCANCODE_ESCAPE]) {
        *quit = true;
    } if (keys[SDL_SCANCODE_DOWN]) {
        *choice = 2;
    } if (keys[SDL_SCANCODE_UP]) {
        *choice = 1;
    } if (keys[SDL_SCANCODE_RETURN] && *choice == 2) {
        *quit = true;
    } if (keys[SDL_SCANCODE_RETURN] && *choice == 1) {
        return true;
    }

    // LIMITE 60 FPS
    SDL_Delay(16);

    return false;
}

void SDLManager::game(bool *quit, Entity* pacman) const
{
    int nbk;
    const Uint8 *keys = SDL_GetKeyboardState(&nbk);
    auto pac = dynamic_cast<Pacman*>(pacman);
    SDL_Event event;

    while (!*quit && SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                *quit = true;
                break;
            default:
                break;
        }
    }

    // Keyboard
    if (keys[SDL_SCANCODE_ESCAPE]) {
        *quit = true;
    } if (keys[SDL_SCANCODE_DOWN]) {
        pac->setRequestDirection(Direction::Down);
    } if (keys[SDL_SCANCODE_UP]) {
        pac->setRequestDirection(Direction::Up);
    } if (keys[SDL_SCANCODE_LEFT]) {
        pac->setRequestDirection(Direction::Left);
    } if (keys[SDL_SCANCODE_RIGHT]) {
        pac->setRequestDirection(Direction::Right);
    }

    // LIMITE 60 FPS
    SDL_Delay(16);
}