#include "game.hpp"

Game::Game():
        window_{ make_unique<Window>() },
        sdlManager_{ make_unique<SDLManager>() },
        scene_{ make_unique<Scene>() }
{}

void Game::start()
{
    bool quit = false;
    int choice = 1;

    while (!quit) {
        if (sdlManager_->menu(&quit, &choice)) {
            launchGame();
        }

        window_->showMenu(choice, scene_->getHighScore());
    }
}

void Game::launchGame()
{
    bool quit = false;

    window_->startAnimation();
    scene_->start();

    while (!scene_->stop() && !quit) {
        sdlManager_->game(&quit, scene_->getEntity("pacman"));
        scene_->updateEntityPosition();
        scene_->checkCollision();

        window_->showMap(*scene_);

        if (scene_->getMap().areAllPelletsCollected()) {
            window_->winAnimation();
            scene_->nextLevel();
        }
    }
    // Delay before returning to the menu
    SDL_Delay(250);
}
