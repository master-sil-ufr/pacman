# Pac-Man

## Presentation
The Advanced Programming course project involves creating a new version of Namco's "PAC-MAN" game (1980).

The programming must be done in C++11/14/17/20 using the SDL2 library, with no other libraries allowed. The display should be done exclusively through sprite copying from a Bitmap image. The code must compile on all systems.

This project was completed by Filipe AUGUSTO and Luc BURCKEL under the supervision of Anna OUSKOVA LEONTEVA.

## Command lines

### Generate build files & run the PacMan game
This script generates build files in the 'build' folder, creates the binary './PacMan' and executes it
```sh
./run.sh
```

### Generate build files:
This script generates build files in the 'build' folder.
```sh
./build.sh
```
or 
```sh
mkdir -p build
cd build
cmake ..
```

### Creating the executable:
Change directory to the build directory using the ```cd build``` command.
```sh
make
```

### Run the PacMan game:
```sh
./PacMan
```

### Generate the documentation using the Doxyfile:
The documentation can be found in **docs/doxygen/**.
```sh
make doc
```

### Remove the generated Doxygen documentation :
```sh
make doxygenclean
```

## Game controls

### Menu:
- **Escape**: closes the current window and terminates the process.
- **Enter/Return**: depending on where the cursor is, the key validates the associated action.
- **Directional arrows**: allow you to navigate through the menu.

![menu](assets/images/menu.png)

### Game:
- **Escape**: brings you back to the menu.
- **Directional arrows**: allow you to move Pac-Man.

![menu](assets/images/game.png)